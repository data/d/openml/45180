# OpenML dataset: hailfinder_5

https://www.openml.org/d/45180

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Hailfinder Bayesian Network. Sample 5.**

bnlearn Bayesian Network Repository reference: [URL](https://www.bnlearn.com/bnrepository/discrete-large.html#hailfinder)

- Number of nodes: 56

- Number of arcs: 66

- Number of parameters: 2656

- Average Markov blanket size: 3.54

- Average degree: 2.36

- Maximum in-degree: 4

**Authors**: B. Abramson, J. Brown, W. Edwards, A. Murphy, and R. L. Winkler.

**Please cite**: ([URL](https://www.sciencedirect.com/science/article/pii/0169207095006648)): B. Abramson, J. Brown, W. Edwards, A. Murphy, and R. L. Winkler. Hailfinder: A Bayesian system for forecasting severe weather. International Journal of Forecasting, 12(1):57-71, 1996.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45180) of an [OpenML dataset](https://www.openml.org/d/45180). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45180/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45180/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45180/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

